Repositórios oficiais do programa "SearchMonkey"

(Atual)

https://salsa.debian.org/debian/searchmonkey
https://salsa.debian.org/debian/searchmonkey/-/tree/master/po?ref_type=heads

(Atingo)

https://sourceforge.net/p/searchmonkey/GTK/ci/master/tree/
https://sourceforge.net/p/searchmonkey/GTK/ci/master/tree/po/


Traduções revisadas por marcelocripe:

https://salsa.debian.org/marcelocripe/searchmonkey

https://gitlab.com/marcelocripe/SearchMonkey/-/blob/main/SearchMonkey_Salsa_Debian_pt_BR_17-03-2023.po

https://gitlab.com/marcelocripe/SearchMonkey/-/blob/main/for_use_antix-contribs_searchmonkey_pt_BR_17-03-2023.po

https://gitlab.com/marcelocripe/SearchMonkey/-/blob/main/searchmonkey.desktop


Para utilizar o arquivo "SearchMonkey_Salsa_Debian_pt_BR_17-03-2023.po", "for_use_antix-contribs_searchmonkey_pt_BR_17-03-2023.po" e o "searchmonkey.desktop", inicie o Emulador de Terminal na pasta onde estão os arquivos que foram baixados e aplique os comandos.

"SearchMonkey_Salsa_Debian_pt_BR_17-03-2023.po" ou "for_use_antix-contribs_searchmonkey_pt_BR_17-03-2023.po":


Comando para converter o arquivo editável da tradução com a extensão ".po" para ".mo".

$ msgfmt SearchMonkey_Salsa_Debian_pt_BR_17-03-2023.po -o searchmonkey.mo

ou

$ msgfmt for_use_antix-contribs_searchmonkey_pt_BR_17-03-2023.po -o searchmonkey.mo


Comando para copiar o arquivo da tradução com a extensão ".mo" para a pasta do idioma "pt_BR".

$ sudo cp searchmonkey.mo /usr/share/locale/pt_BR/LC_MESSAGES



"searchmonkey.desktop":

Comando para copiar o arquivo com a extensão ".desktop" para a pasta /usr/share/applications.

$ sudo cp searchmonkey.desktop /usr/share/applications

Comando para escrever globalmente todas as entradas dos menus do antiX:
$ sudo desktop-menu --write-out-global